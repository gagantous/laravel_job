<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model{

    protected $guarded = [
        'id_users', 
        'last_update'
    ];


    public $timestamps = false;
    protected $table = "products";

   
    // protected dates = []
}
