<?php

namespace App\Http\Controllers\Product;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;
use DB;

class ProductController extends Controller
{
    public function index(){
		$data['product'] = DB::table('products AS a')
			->join('product_kinds AS b','a.id_kind_products','=','b.id')
			->join('users AS c','a.id_users','=','c.id','left outer')
			->select('a.*','b.kindproduct_name','c.surname')
			->get();
		return view('product/V_index_product',$data);
	}

	public function tambah(){
		return view('product/V_tambah_product');
	}

	public function insert(Request $request){
		// $data = new Product()
		$this->validate($request, [
			"product_name" => "required|",
			// "kind_product" => "required",
			"product_price" => "required|integer",
			"product_price_promo" => "integer|min:0",
			"product_stock" => "integer"
		]);
		$product = new Product();
		$product->products_name = $request->input('product_name');
		$product->products_price = $request->input('product_price');
		$product->products_stock = $request->input('product_stock');
		$product->id_kind_products = 2;
		$product->created_at = date('Y-m-d H:i:s');
		// $product->id_kind_products = $request->input('id_kind_products');
		$product->products_description = $request->input('product_description');
		$product->isPromo = $request->input('promostat');
		$product->id_users = 4;
		$product->products_promo_prices = $request->input('product_price_promo');

		$saved = $product->save();
		if(!$saved){
			$mes = "Produk gagal di tambahkan";
			$stat = "danger";
		}else{
			$mes = "Produk berhasil ditambahkan";
			$stat = "success";
		};

		return redirect('/product')->with([
			"messages" => $mes,
			"stat" => $stat,
		]);

	}

	public function ubah($parameter){
		 $data['product'] = DB::table('products')
		->where('id',$parameter)
		->limit(1)
		->get();
		// print_r($data);
		// dd($data);
		return view('product/V_ubah_product',$data);
	}

	public function update(Request $request){
		$arr = array(
			'products_name' => $request->input('product_name'),
			'products_price' => $request->input('product_price'),
			'products_stock' => $request->input('product_stock'),
			'created_at' => date('Y-m-d H:i:s'),
			'products_description' => $request->input('product_description'),
			'isPromo' => $request->input('promostat'),
			'id_users' => 4,
			'products_promo_prices' =>  $request->input('product_price_promo')
		);
		$id= $request->input('idProduct');
		$insert = DB::table('products')->where('id',$id)->update($arr);
		if(!$insert){
			$mes = "Produk  {$request->input('product_name')} gagal di update/ubah";
			$stat = "danger";
		}else{
			$mes = "Produk {$request->input('product_name')} berhasil di update/ubah";
			$stat = "success";
		};

		return redirect('/product')->with([
			"messages" => $mes,
			"stat" => $stat,
		]);

	}

	public function view($parameter){
		$data['product'] = DB::table('products')
	   ->where('id',$parameter)
	   ->limit(1)
	   ->get();
	   // print_r($data);
	   // dd($data);
	   return view('product/V_view_product',$data);
   }
	
   public function delete($param){
		$query = DB::table('products')
		->where('id',$param)
		->delete();
		if(!$query){
			$mes = "Produk gagal dihapus";
			$stat = "danger";
		}else{
			$mes = "Produk berhasil dihapus";
			$stat = "success";
		};

		return redirect('/product')->with([
			"messages" => $mes,
			"stat" => $stat,
		]);
	}


}
