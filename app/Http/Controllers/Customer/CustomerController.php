<?php

namespace App\Http\Controllers\Customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Customer;
use DB;

class CustomerController extends Controller
{
	public function index(){
		$data['customer'] = DB::table('customers')
			->get();
		return view('customers/V_index_customer',$data);
	}

	public function tambah(){
		return view('customers/V_tambah_customer');
	}

	public function insert(Request $request){
		$this->validate($request, [
			"nama_customer" => "required|string",
			"alamat" => "required|string",
			"no_hp" => "integer"
		]);
		$customer = DB::table('customers')
		->insert([
			'nama_customer' => $request->input('nama_customer'),
			'alamat' => $request->input('alamat'),
			'no_hp' => $request->input('no_hp')
		]);
		if(!$customer){
			$mes = "Produk gagal di tambahkan";
			$stat = "danger";
		}else{
			$mes = "Produk berhasil ditambahkan";
			$stat = "success";
		};

		return redirect('/customer')->with([
			"messages" => $mes,
			"stat" => $stat,
		]);

	}

	public function ubah($parameter){

		 $data['customer'] = DB::table('customers')
		->where('id',$parameter)
		->limit(1)
		->get();
		// print_r($data);
		// dd($data);
		return view('customers/V_ubah_customer',$data);
	}

	public function update(Request $request){
		$arr = array(
			'nama_customer' => $request->input('nama_customer'),
			'alamat' => $request->input('alamat'),
			'no_hp' => $request->input('no_hp')
		);
		$id = $request->input('idCustomer');
		$insert = DB::table('customers')->where('id',$id)->update($arr);
		if(!$insert){
			$mes = "Customer  {$request->input('nama_customer')} gagal di update/ubah";
			$stat = "danger";
		}else{
			$mes = "Customer {$request->input('nama_customer')} berhasil di update/ubah";
			$stat = "success";
		};

		return redirect('/customer')->with([
			"messages" => $mes,
			"stat" => $stat,
		]);

	}

	public function view($parameter){
		$data['customer'] = DB::table('customers')
	   ->where('id',$parameter)
	   ->limit(1)
	   ->get();
	   // print_r($data);
	   // dd($data);
	   return view('customers/V_view_customer',$data);
   }
	
   public function delete($param){
		$query = DB::table('customers')
		->where('id',$param)
		->delete();
		if(!$query){
			$mes = "Produk gagal dihapus";
			$stat = "danger";
		}else{
			$mes = "Produk berhasil dihapus";
			$stat = "success";
		};

		return redirect('/customer')->with([
			"messages" => $mes,
			"stat" => $stat,
		]);


	}
}
