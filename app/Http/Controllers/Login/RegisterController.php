<?php

namespace App\Http\Controllers\Login;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Register extends Controller
{
    public function index(){
    	return view('log_reg/login');
    }

     public function postRegister(){
    	$this->validate($request, [
			"nama_customer" => "required|string",
			"alamat" => "required|string",
			"no_hp" => "integer"
		]);
		$customer = DB::table('customers')
		->insert([
			'nama_customer' => $request->input('nama_customer'),
			'alamat' => $request->input('alamat'),
			'no_hp' => $request->input('no_hp')
		]);
    }
}
