<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller{

	public function home(){
		$data['hobi'] = array('membaca buku','Musik','Belanja');
		$data['judul'] = "Welcome";
	    return view('dashboard/V_index_dashboard', $data);
	}
    
}
