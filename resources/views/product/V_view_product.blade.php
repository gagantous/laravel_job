
@extends('layout/layout')


@section('css_custom')
    <link href="{{ asset('gentelella_assets/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('gentelella_assets/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('gentelella_assets/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('gentelella_assets/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('gentelella_assets/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('gentelella_assets/vendors/switchery/dist/switchery.min.css') }}" rel="stylesheet">
@endsection


@section('js_custom')
  <script src="{{ asset('gentelella_assets/vendors/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('gentelella_assets/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('gentelella_assets/vendors/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('gentelella_assets/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js') }}"></script>
    <script src="{{ asset('gentelella_assets/vendors/datatables.net-buttons/js/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('gentelella_assets/vendors/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('gentelella_assets/vendors/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('gentelella_assets/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js') }}"></script>
    <script src="{{ asset('gentelella_assets/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js') }}"></script>
    <script src="{{ asset('gentelella_assets/vendors/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('gentelella_assets/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js') }}"></script>
    <script src="{{ asset('gentelella_assets/vendors/datatables.net-scroller/js/dataTables.scroller.min.js') }}"></script>
    <script src="{{ asset('gentelella_assets/vendors/jszip/dist/jszip.min.js') }}"></script>
    <script src="{{ asset('gentelella_assets/vendors/pdfmake/build/pdfmake.min.js') }}"></script>
    <script src="{{ asset('gentelella_assets/vendors/pdfmake/build/vfs_fonts.js') }}"></script>

    <script src="{{ asset('gentelella_assets/vendors/google-code-prettify/src/prettify.js') }}"></script>
    <!-- jQuery Tags Input -->
    <script src="{{ asset('gentelella_assets/vendors/jquery.tagsinput/src/jquery.tagsinput.js') }}"></script>
    <!-- Switchery -->
    <script src="{{ asset('gentelella_assets/vendors/switchery/dist/switchery.min.js') }}"></script>
    <!-- Select2 -->
    <script src="{{ asset('gentelella_assets/vendors/select2/dist/js/select2.full.min.js') }}"></script>
    <!-- Parsley -->
    <script src="{{ asset('gentelella_assets/vendors/parsleyjs/dist/parsley.min.js') }}"></script>
    <!-- Autosize -->
    <script src="{{ asset('gentelella_assets/vendors/autosize/dist/autosize.min.js') }}"></script>
    <!-- jQuery autocomplete -->
    <script src="{{ asset('gentelella_assets/vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js') }}"></script>
    <!-- starrr -->
    <script src="{{ asset('gentelella_assets/vendors/starrr/dist/starrr.js') }}"></script>
@endsection

@section('content')
  <div class="right_col" role="main">
            <div class="page-title">
              <div class="title_left">
                <h3>DETAIL PRODUK <small>Manajemen Data Produk</small></h3>
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  
                  <div class="x_content">	
                   <div id="demo-form2" method="post" action="{{ url('product/update') }}"  class="form-horizontal form-label-left">
                          {{ csrf_field()}}

                            @if($errors->any())
                              <div class="alert alert-danger">
                                <ul>
                                  @foreach($errors->all() as $errors)
                                    <li>
                                        {{ $errors }}
                                    </li>
                                  @endforeach
                                </ul>
                              </div>
                            @endif 
                            @foreach($product as $product)
                              <input type="hidden" name="idProduct"/>
                             
                              <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="product_name">Nama Produk <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                {{$product->products_name}}
                                </div>
                              </div>
                              <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="kind_product">Jenis Produk <span class="required">*</span>
                                </label>
                                <div class="col-md-3 col-sm-4 col-xs-12">
                                {{$product->id_kind_products}}
                                </div>
                              </div>
                              <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="product_price">Harga Produk <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                {{$product->products_price}}
                                </div>
                              </div>
                              <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Jumlah Stok <span class="required">*</span>
                                </label>
                                <div class="col-md-3 col-sm-4 col-xs-12">
                                {{$product->products_stock}}
                                </div>
                              </div>
                              <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Promo Barang ?</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <p>
                                  @if($product->isPromo == TRUE)
                                    Ya
                                  @else
                                    Tidak
                                  @endif
                                  </p>
                                </div>
                              </div>
                              <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="product_price">Harga Promo
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                {{(int) $product->products_promo_prices }}
                                </div>
                              </div>
                              <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="product_description">Deskripsi Produk
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                {{$product->products_description}}
                                </div>
                              </div>
                              @endforeach
                              <div class="clearfix"></div>
                              <hr/>
                      </div>         
                  </div>
                </div>
              </div>
            </div>
          </div>

@endsection

@section('custom_javascript')
    <script>

      $('#tambah_product').on('click',function(){
        $('#modalproduk').modal('show');
        $('#myModalProdukLabel').html("").html("TAMBAH DATA PRODUK");
        $('#demo-form2').parsley().reset();
        $('#formProdukSubmit').data('action','add');
      })  

      $('#ubah_product').on('click',function(){
        $('#modalproduk').modal('show');
        $('#myModalProdukLabel').html("").html("UBAH DATA PRODUK");
        $('#demo-form2').parsley().reset();
         $('#formProdukSubmit').data('action','edit');
      })    

      $('#formProdukSubmit').on('click', function() {
        let fomrArr = $('#demo-form2').serializeArray();
        let tblsubmit = $('#formProdukSubmit');
        console.log(fomrArr);
        var url = "";
        if ( !$('#demo-form2').parsley().validate() ){
          $('#demo-form2').parsley().validate();
          validateFront();
          alert('Belum');
        }else{
          if (tblsubmit.data('action') == "add"){
            url = "{{ url('/product/add') }}";
            insertProduct(url, fomrArr);
            alert('add')
          }else if (tblsubmit.data('action') == "edit"){
            url = "{{ url('/website') }}";
            updateProduct(url, fomrArr);
            alert('edi');
          }    


        }
      });

      $('#formProdukReset').on('click', function() {
        $('#demo-form2')[0].reset();
      });

      var validateFront = function() {
        if (true === $('#demo-form2').parsley().isValid()) {
        $('.bs-callout-info').removeClass('hidden');
        $('.bs-callout-warning').addClass('hidden');
        } else {
        $('.bs-callout-info').addClass('hidden');
        $('.bs-callout-warning').removeClass('hidden');
        }
      };

      var getSpecProduct = function(){

      }

      var updateProduct = function(url,data){
        $.ajax({
          url:url,
          method: "POST",
          data: data,
          dataType: "json",
          beforeSend: function(response){

          },
          success: function(data){

          },
          error: function(data){

          },
        })
      }

      var insertProduct = function(url,data){
        $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
        $.ajax({
          url:"{{ url('/product/add') }}",
          method: "POST",
          data:{
            '_'
            'name': $('#product_name').val()
          },
          dataType: "json",
          beforeSend: function(response){
            console.log(response)
          },
          success: function(result){
            console.log(result);
          },
          error: function(data){
            console.log(data);
          },

        })

        
      }

      var deleteProduct = function(){

      }

      

      

    </script>

@endsection
