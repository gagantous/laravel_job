
@extends('layout/layout')


@section('css_custom')
    <link href="{{ asset('gentelella_assets/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('gentelella_assets/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('gentelella_assets/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('gentelella_assets/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('gentelella_assets/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('gentelella_assets/vendors/switchery/dist/switchery.min.css') }}" rel="stylesheet">
@endsection


@section('js_custom')
  <script src="{{ asset('gentelella_assets/vendors/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('gentelella_assets/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('gentelella_assets/vendors/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('gentelella_assets/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js') }}"></script>
    <script src="{{ asset('gentelella_assets/vendors/datatables.net-buttons/js/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('gentelella_assets/vendors/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('gentelella_assets/vendors/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('gentelella_assets/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js') }}"></script>
    <script src="{{ asset('gentelella_assets/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js') }}"></script>
    <script src="{{ asset('gentelella_assets/vendors/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('gentelella_assets/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js') }}"></script>
    <script src="{{ asset('gentelella_assets/vendors/datatables.net-scroller/js/dataTables.scroller.min.js') }}"></script>
    <script src="{{ asset('gentelella_assets/vendors/jszip/dist/jszip.min.js') }}"></script>
    <script src="{{ asset('gentelella_assets/vendors/pdfmake/build/pdfmake.min.js') }}"></script>
    <script src="{{ asset('gentelella_assets/vendors/pdfmake/build/vfs_fonts.js') }}"></script>

    <script src="{{ asset('gentelella_assets/vendors/google-code-prettify/src/prettify.js') }}"></script>
    <!-- jQuery Tags Input -->
    <script src="{{ asset('gentelella_assets/vendors/jquery.tagsinput/src/jquery.tagsinput.js') }}"></script>
    <!-- Switchery -->
    <script src="{{ asset('gentelella_assets/vendors/switchery/dist/switchery.min.js') }}"></script>
    <!-- Select2 -->
    <script src="{{ asset('gentelella_assets/vendors/select2/dist/js/select2.full.min.js') }}"></script>
    <!-- Parsley -->
    <script src="{{ asset('gentelella_assets/vendors/parsleyjs/dist/parsley.min.js') }}"></script>
    <!-- Autosize -->
    <script src="{{ asset('gentelella_assets/vendors/autosize/dist/autosize.min.js') }}"></script>
    <!-- jQuery autocomplete -->
    <script src="{{ asset('gentelella_assets/vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js') }}"></script>
    <!-- starrr -->
    <script src="{{ asset('gentelella_assets/vendors/starrr/dist/starrr.js') }}"></script>
@endsection

@section('content')
  <div class="right_col" role="main">
            <div class="page-title">
              <div class="title_left">
                <h3>PRODUK <small>Manajemen Data Produk</small></h3>
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <ul class="nav navbar-right panel_toolbox">
                      <a href="{{ url('customer/tambah') }}" class="btn btn-primary btn-small pull-right" id="tambah_product"><i class="fa fa-plus"></i> Tambah</a>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  @if(session('messages'))
                  <div class="alert alert-{{session('stat')}}" >
                         {{session('messages')}}
                  </div>
                  @endif
                  <div class="x_content">
                    <!-- <p class="text-muted font-13 m-b-30">
                      Responsive is an extension for DataTables that resolves that problem by optimising the table's layout for different screen sizes through the dynamic insertion and removal of columns from the table.
                    </p> -->
					
                    <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Nama Customer</th>
                          <th width="15%">Alamat Customer</th>
                          <th>No. Hp</th>
                          <th width="10%">Lanjut</th>
                        </tr>
                      </thead>
                      <tbody>
                        @php ($a = 1)
                        @foreach ($customer->all() as $customer)
                            <tr>
                              <td>{{$a}}</td>
                              <td>{{$customer->nama_customer}}</td>
                              <td>{{$customer->alamat}}</td>
                              <td>{{$customer->no_hp}}</td>
                              <td>
                                <a href="/customer/view/{{$customer->id}}" class="btn btn-primary btn-xs"><i class="fa fa-folder"></i> View </a>
                                <a href="/customer/ubah/{{$customer->id}}" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Edit </a>
                                <a href="/customer/delete/{{$customer->id}}" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Delete </a>
                              </td>
                            </tr>
                            @php ($a++)
                        @endforeach
                      </tbody>
                    </table>
					
					
                  </div>
                </div>
              </div>
            </div>
          </div>

@endsection

