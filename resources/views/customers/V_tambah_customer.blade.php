
@extends('layout/layout')


@section('css_custom')
    <link href="{{ asset('gentelella_assets/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('gentelella_assets/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('gentelella_assets/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('gentelella_assets/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('gentelella_assets/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('gentelella_assets/vendors/switchery/dist/switchery.min.css') }}" rel="stylesheet">
@endsection


@section('js_custom')
  <script src="{{ asset('gentelella_assets/vendors/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('gentelella_assets/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('gentelella_assets/vendors/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('gentelella_assets/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js') }}"></script>
    <script src="{{ asset('gentelella_assets/vendors/datatables.net-buttons/js/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('gentelella_assets/vendors/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('gentelella_assets/vendors/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('gentelella_assets/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js') }}"></script>
    <script src="{{ asset('gentelella_assets/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js') }}"></script>
    <script src="{{ asset('gentelella_assets/vendors/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('gentelella_assets/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js') }}"></script>
    <script src="{{ asset('gentelella_assets/vendors/datatables.net-scroller/js/dataTables.scroller.min.js') }}"></script>
    <script src="{{ asset('gentelella_assets/vendors/jszip/dist/jszip.min.js') }}"></script>
    <script src="{{ asset('gentelella_assets/vendors/pdfmake/build/pdfmake.min.js') }}"></script>
    <script src="{{ asset('gentelella_assets/vendors/pdfmake/build/vfs_fonts.js') }}"></script>

    <script src="{{ asset('gentelella_assets/vendors/google-code-prettify/src/prettify.js') }}"></script>
    <!-- jQuery Tags Input -->
    <script src="{{ asset('gentelella_assets/vendors/jquery.tagsinput/src/jquery.tagsinput.js') }}"></script>
    <!-- Switchery -->
    <script src="{{ asset('gentelella_assets/vendors/switchery/dist/switchery.min.js') }}"></script>
    <!-- Select2 -->
    <script src="{{ asset('gentelella_assets/vendors/select2/dist/js/select2.full.min.js') }}"></script>
    <!-- Parsley -->
    <script src="{{ asset('gentelella_assets/vendors/parsleyjs/dist/parsley.min.js') }}"></script>
    <!-- Autosize -->
    <script src="{{ asset('gentelella_assets/vendors/autosize/dist/autosize.min.js') }}"></script>
    <!-- jQuery autocomplete -->
    <script src="{{ asset('gentelella_assets/vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js') }}"></script>
    <!-- starrr -->
    <script src="{{ asset('gentelella_assets/vendors/starrr/dist/starrr.js') }}"></script>
@endsection

@section('content')
  <div class="right_col" role="main">
            <div class="page-title">
              <div class="title_left">
                <h3>TAMBAH Customer <small>Manajemen Data Customer</small></h3>
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  
                  <div class="x_content">	
                   <form id="demo-form2" method="post" action="{{ url('customer/insert') }}"  class="form-horizontal form-label-left">
                          {{ csrf_field()}}

                            @if($errors->any())
                              <div class="alert alert-danger">
                                <ul>
                                  @foreach($errors->all() as $errors)
                                    <li>
                                        {{ $errors }}
                                    </li>
                                  @endforeach
                                </ul>
                              </div>
                            @endif
                              <input type="hidden" name="idProduct"/>
                              
                              <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama_customer">Nama Customer <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <input type="text" id="nama_customer" name="nama_customer" val require class="form-control col-md-7 col-xs-12">
                                </div>
                              </div>
                              <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="alamat">Alamat <span class="required">*</span>
                                </label>
                                <div class="col-md-3 col-sm-4 col-xs-12">
                                  <input type="text" id="alamat" name="alamat" class="form-control col-md-7 col-xs-12">
                                </div>
                              </div>
                              <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="no_hp">No. Hp 
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <input type="text" id="no_hp" name="no_hp"  class="form-control col-md-7 col-xs-12">
                                </div>
                              </div>
                              <div class="clearfix"></div>
                              <hr/>
                              <div class="row">

                                  <button class="pull-right btn btn-primary" type="submit" name="submit"><i class="fa fa-pen"></i> SUBMIT</button>
                                  <a href="/product" class="pull-left btn btn-danger"><i class="fa fa-cross"></i> BATAL</a>
                              </div>
                      </form>         
                  </div>
                </div>
              </div>
            </div>
          </div>

@endsection
