<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductKindsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('products_kinds')):   
            Schema::create('product_kinds', function (Blueprint $table) {
                $table->increments('id');
                $table->string('kindproduct_name');
                $table->dateTime('created_at');
                $table->timestamp('last_update');
            });
        else:
            return false;
        endif;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_kinds');
    }
}
