<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('products')):
            Schema::create('products', function (Blueprint $table) {
                $table->increments('id');
                $table->string('products_name',50);
                $table->decimal('products_price',15,0);
                $table->integer('products_stock')->nullable();
                $table->integer('id_kind_products')->nullable();
                $table->text('products_description')->nullable();
                $table->boolean('isPromo');
                $table->decimal('products_promo_prices')->nullable();
                $table->integer('id_users');
                $table->dateTime('created_at');
                $table->timestamp('last_updated');
            });
        else: 
            return false;
        endif;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
