<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCheckoutOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('checkout_orders')):
            Schema::create('checkout_orders', function (Blueprint $table) {
                $table->increments('id');
                $table->string('transaction_nota_order',40);
                $table->decimal('paid_total');
                $table->timestamp('checkout_date');
            });
        else:
            return false;
        endif;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('checkout_orders');
    }
}
