<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('users')):
            Schema::create('users', function (Blueprint $table) {
                $table->increments('id');
                $table->string('username',50)->unique();
                $table->string('surname');
                $table->string('email')->unique();
                $table->string('password');
                $table->string('no_hp',32);         
                $table->rememberToken();
                $table->dateTime('created_at');
                $table->timestamp('last_updated');
                $table->enum('role', ['user', 'admin', 'superadmin'])->default('user');
            });

        else:
            return true;
        endif;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
