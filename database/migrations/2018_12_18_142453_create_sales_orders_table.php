<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('sales_orders')):
            Schema::create('sales_orders', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('id_customers');
                $table->integer('id_users');
                $table->integer('id_products');
                $table->string('transaction_nota',40);
                $table->integer('quantity_products');
                $table->dateTime('created_at');
                $table->timestamp('last_updated');            
        });
        else:
            return false;
        endif;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales_orders');
    }
}
