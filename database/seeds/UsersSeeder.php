<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('id_ID');
        // $faker->seed(4);
        $limit = 5;
        $role = array("user",'admin','superadmin');
        for ($i=0; $i < $limit; $i++) { 
    	    DB::table('users')->insert([
             'username' => $faker->userName,
             'surname' => $faker->name,
             'email' => $faker->freeEmail,
             'password' => '123456789',
             'no_hp' => $faker->e164phoneNumber,
             'created_at' => date('y-m-d'),
             'last_updated' => date('y-m-d H:i:s'),
             'role' => rand(0,2)
        	]);
        }
    }
}
