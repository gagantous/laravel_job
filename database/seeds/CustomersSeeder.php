<?php

use Illuminate\Database\Seeder;

class CustomersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('id_ID');
        // $faker->seed(4);
        $limit = 5;

        for ($i=0; $i < $limit; $i++) { 
    	DB::table('customers')->insert([
    		'nama_customer' => $faker->name,
    		'alamat' => $faker->address,
    		'no_hp' => $faker->e164PhoneNumber,
    		'created_at' => $faker->dateTimeBetween($startDate = '-10 years', $endDate = 'now'),
        	]);
        }
    }
}
