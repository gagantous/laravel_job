<?php

use Illuminate\Database\Seeder;
// use  as FakerFact;

class ProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('id_ID');
        // $faker->seed(4);
        $limit = 30;

        for ($i=0; $i < $limit; $i++) { 
    	DB::table('products')->insert([
    		'products_name' => $faker->name,
    		'products_price' => $faker->numberBetween($min = 100000, $max = 90000),
    		'products_stock' => $faker->numberBetween(15,40),
    		'id_kind_products' => $faker->numberBetween(1,10),
    		'products_description' => $faker->text,
    		'isPromo' => $faker->numberBetween(0,1),
    		'products_promo_prices' => $faker->numberBetween($min = 10000, $max = 900000),
			'id_users' => $faker->numberBetween(1,10),
			'created_at' => $faker->dateTimeBetween($startDate = '-10 years', $endDate = 'now')
        	]);
        }

    }
}
