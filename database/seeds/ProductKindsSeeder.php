<?php

use Illuminate\Database\Seeder;

class ProductKindsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $faker = Faker\Factory::create('id_ID');
        $limit = 20;
        $objJenis = array(
        	'Perabotan rumah',
        	'Alat mandi',
        	'Peralatan makan',
        	'Alat berkebun',
        	'Alat kedokteran' 
        );
        for ($i=0; $i < count($objJenis); $i++) { 
    	DB::table('product_kinds')->insert([
    		'kindproduct_name' => $objJenis[$i],
    		'created_at' => $faker->dateTimeBetween($startDate = '-10 years', $endDate = 'now'),
        	]);
        }
    }
}
