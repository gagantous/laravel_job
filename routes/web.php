<?php
/* FOR MODULE PRODUCT
|--------------------------------------
|   ROUTES FOR PRODUCT
|--------------------------------------
| List of route

*/

// FOR Module Dashboard
Route::get('/', 'Dashboard\HomeController@home');
// END OF MODULE DASHBOARD


// For Modul Product

// Route::resource('product', 'Product\ProductController@index');
Route::get('/product', 'Product\ProductController@index')->middleware('auth');;
Route::get('/product/tambah', 'Product\ProductController@tambah');
Route::post('/product/insert', 'Product\ProductController@insert');
Route::get('/product/ubah/{id}','Product\ProductController@ubah');
Route::get('/product/view/{id}','Product\ProductController@view');
Route::post('/product/update','Product\ProductController@update');
Route::get('/product/delete/{id}','Product\ProductController@delete');
// ENd for modul product

// For modul customer
Route::get('/customer', 'Customer\CustomerController@index');
Route::get('/customer/tambah', 'Customer\CustomerController@tambah');
Route::post('/customer/insert', 'Customer\CustomerController@insert');
Route::get('/customer/ubah/{id}','Customer\CustomerController@ubah');
Route::get('/customer/view/{id}','Customer\CustomerController@view');
Route::post('/customer/update','Customer\CustomerController@update');
Route::get('/customer/delete/{id}','Customer\CustomerController@delete');
// end for modul customer

// for modul login
Route::get('/login', 'Login\LoginController@index');
Route::post('/login', 'Login\LoginController@postLogin')->name('login');
// end for modul login

// for modul registration
Route::post('/register', 'Login\RegistrationController@postRegister');
// end for modul registration

// form modul  logout
Route::get('/logout', 'Login\LogoutController@home');
// end for modul logout

// for modul order
Route::get('/order', 'Order\OrderController@index');
// end for modul order


// Route::get('/', function() {
//    return response()->json([
//     'stuff' => phpinfo()
//    ]);
// });



// Route::get('/tentang', function() {
// 	$data['judul'] = "Tentang";
// 	return view('tentang',$data);
// });

// Route::get('/kontak', function() {
// 	$data['judul'] = "Kontak";
// 	return view('contact', $data);
// });

// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');


